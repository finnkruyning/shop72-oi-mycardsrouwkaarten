$(document).ready(function(){	

		if (!$.browser.opera) {
    
			// select element styling
			$('#calculator select').each(function(){
				var title = $(this).attr('title');
				if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
				$(this)
					.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
					.after('<span class="select">' + title + '</span>')
					.change(function(){
						val = $('option:selected',this).text();
						$(this).next().text(val);
						})
			});

		};
$('#type').parent().find('.select').html('Dubbel'); 
$('#orien').parent().find('.select').html('Vierkant');
$('#size').parent().find('.select').html('13 x 13 cm'); 
$('#papertypes').parent().find('.select').html('Natuurkarton');
		
	});
	
	// get type
	function get_type() {
		var type = 'enkel';
		type = document.getElementById("type");
		//if (document.getElementById("single").checked) {
		//		return 'single';
		//	}
		//	if (document.getElementById("double").checked) {
		//		return 'double';	
		//	}
		
		//return type;
		return type.options[type.selectedIndex].value;
	}
	
	// get orien
	function get_orien() {
		var orien = 'vierkant';
		orien = document.getElementById("orien");
		
		
		//alert(orien.options[orien.selectedIndex].value);
		//if (document.getElementById("square").checked) {
	//		return 'square';	
	//	}
	//	if (document.getElementById("portrait").checked) {
	//		return 'portrait';	
	//	}
	//	if (document.getElementById("landscape").checked) {
	//		return 'landscape';	
	//	}
	
		return orien.options[orien.selectedIndex].value;
		
	}
	
	
	
	// get size
	function the_size(val, name) {
		if (name=='orien') {
			$('#size option:selected').attr('selected','');
			var type = get_type();
			if (type == 'enkel') {
				switch(val) {
					case 'vierkant': 
						$('#size option').show(); 
						
						$('#size option').eq(0).hide(); 
						$('#size option').eq(1).hide(); 
						$('#size option').eq(2).hide(); 
						$('#size option').eq(3).hide(); 
						//$('#orien option').show();
						//$('#orien option').eq(0).hide();
						//$('#orien option').eq(2).hide();
						break;
					case 'portrait': 
						$('#size option').hide(); 
						$('#size option').eq(0).show();
						$('#size option').eq(2).show();
						$('#size').parent().find('.select').html('10 x 15 cm'); 
						break;
					case 'liggend': 
						$('#size option').hide(); 
						$('#size option').eq(1).show(); 
						$('#size option').eq(3).show(); 
						break;
				}
			} else {
				switch(val) {
					case 'vierkant': 
						$('#size option').show(); 
						
						$('#size option').eq(0).hide(); 
						$('#size option').eq(1).hide(); 
						$('#size option').eq(2).hide(); 
						$('#size option').eq(3).hide(); 
						$('#size option').eq(6).hide(); 
						//$('#orien option').show();
						//$('#orien option').eq(0).hide();
						//$('#orien option').eq(2).hide();
						break;
					case 'portrait': 
						$('#size option').hide(); 
						$('#size option').eq(0).show(); 
						$('#size option').eq(2).show(); 
						break;
					case 'liggend': 
						$('#size option').hide(); 
						$('#size option').eq(1).show(); 
						$('#size option').eq(3).show();
						break;
				}
			}
		} else {
			var orien = get_orien();
			if (orien == 'vierkant') {
				switch(val) {
					case 'enkel': 
						$('#size option').show(); 
						$('#size option').eq(0).hide(); 
						$('#size option').eq(3).hide(); 
						break;
					case 'dubbel': 
						$('#size option').show(); 
						$('#size option').eq(0).hide(); 
						break;
				}
			} else {
				if (orien == 'portrait') {
					switch(val) {
						case 'enkel': $('#size option').show(); $('#size option').eq(3).hide(); break;
						case 'dubbel': $('#size option').show(); break;
					}
				} else {
					switch(val) {
						case 'enkel': $('#size option').show(); $('#size option').eq(3).hide(); break;
						case 'dubbel': $('#size option').show(); break;
					}
				}
			}
		}
	}
	
	
	// function radio checked
	function check(arg) {
		var a = arg.split('|');
		for (i=0;i<a.length;i++) {
			if (document.getElementById(a[i]).onchange) 
				return document.getElementById(a[i]).value;
		}
		
		//type1 = document.getElementById(a[i]);
				//return type1.options[type1.selectedIndex].value;
		
		//type = document.getElementById("type");
		//if (document.getElementById("single").checked) {
		//		return 'single';
		//	}
		//	if (document.getElementById("double").checked) {
		//		return 'double';	
		//	}
		
		//return type;
		//return type.options[type.selectedIndex].value;
	}




	function placeHolder(element){
		var standard_message = $(element).val();
		$(element).focus(
			function() {
				if ($(this).val() == standard_message)
					$(this).val("");
			}
		);
		$(element).blur(
			function() {
				if ($(this).val() == "")
					$(this).val(standard_message);
			}
		);
	}


	function posAmountCard(n) {
		if (n==0) {
			return 4;
		} else if (n==1) {
			return 4;
		} else if (n>1 && n<10) {
			return 5;
		} else if (n>9 && n<50) {
			return 6;
		} else if (n>49 && n<100) {
			return 7;
		} else if (n>99 && n<200) {
			return 8;
		} else if (n>199) {
			return 9;	
		}
	}


	function posPaperType(s) {
		switch (s) {
			case 'fotokaart': return 10; break;
			case 'natuurkarton': return 11; break;
			case 'silk-mc': return 12; break;
			case 'parelmoer': return 13; break;
			case 'oud-hollands': return 14; break;	
			//case 'enveloppen': return 14; break;
		}
	}


	function posShip(n) {
		if (n==0) {
			return 16;
		} else if (n==1) {
			return 16;
		} else if (n>1 && n<20) {
			return 17;
		} else if (n>19) {
			return 18;
		}
	}


function calprice(s)
	{
		jQuery('#message p').remove();
		var num_card = jQuery('#amount-card').val();
		var num_envelop = jQuery('#amount-envelop').val();
		
		//var size = jQuery('#size').val();
		var size = jQuery('#size').parent().find('.select').html();
		switch(size) {
			case '10 x 15 cm': size = 'postkaart'; break;
			case '11 x 17 cm': size = 'normaal'; break;
			case '15 x 10 cm': size = 'postkaart01'; break;
			case '17 x 11 cm': size = 'normaal01'; break;
			case '8 x 8 cm': size = 'extra klein'; break;
			case '11 x 11 cm': size = 'klein'; break;
			case '13 x 13 cm': size = 'normaal02'; break;
			case '15 x 15 cm': size = 'groot'; break;
		}
		
		//var papertype = jQuery('#papertypes').val();
		var papertype = jQuery('#papertypes').parent().find('.select').html();
		switch(papertype) {
			case 'Fotokaart': papertype = 'fotokaart'; break;	
			case 'Natuurkarton': papertype = 'natuurkarton'; break;	
			case 'Silk-mc': papertype = 'silk-mc'; break;	
			case 'Parelmoer': papertype = 'parelmoer'; break;	
			case 'Oud-hollands': papertype = 'oud-hollands'; break;	
		}
		
		//var orien = get_orien();  //check('square|portrait|landscape');
		var orien = jQuery('#orien').parent().find('.select').html();
		switch(orien) {
			case 'Vierkant': orien = 'vierkant'; break;
			case 'Staand': orien = 'staand'; break;
			case 'Liggend': orien = 'liggend'; break;
		}
		
		//var type = get_type(); //check('single|double');
		var type = jQuery('#type').parent().find('.select').html();
		switch(type) {
			case 'Enkel': type = 'enkel'; break;
			case 'Dubbel': type = 'dubbel'; break;
		}
		
		var message = true;
		var data_arr = [];
		
		//alert('type : '+type);
		//alert('orien : '+orien);
		//alert('size : '+size);
		if (s == "first") {
			type = 'dubbel';
			orien = 'vierkant';
			size = 'normaal02';
			papertype = 'natuurkarton';
			num_card = 50;
			num_envelop = 50;
		} 
		if (s == 'last') {
			if (size == '') { jQuery('#message').show(); jQuery('#message').text('Nog niet alles is ingevuld.'); message = false; }
			if (papertype == '') { jQuery('#message').show(); jQuery('#message').text('Nog niet alles is ingevuld.'); message = false; }
			if (num_card == '') { jQuery('#message').show(); jQuery('#message').text('Nog niet alles is ingevuld.'); message = false; }
		}
		
		if (message) {
			jQuery('#message').css('display','none');
			jQuery.get('/css/pricesheet-mycard.csv', function(data) {
				data = data.split(',end');
				//alert(data);
				for (i=0;i<data.length;i++) 
				{ 
					data_arr[i] = data[i].split(','); 
				}
				
				var group01 = type + ',' + orien + ',' + size;
				//alert(group01);
				
				for (j=0;j<data.length;j++) 
				{ 
					//alert(data[j].indexOf(group01));
					if (data[j].indexOf(group01) != -1) 
					{ 
						var pos01 = j; 
						break; 
					} 
					
					
				}

				
				var pos02 = posAmountCard(num_card);
				var pos03 = posPaperType(papertype);
				var pos04 = posShip(num_card);
				if (num_envelop == '') num_envelop = num_card;
				
				var amount_card = num_card*data_arr[pos01][pos02];
				var price_papertype = num_card*data_arr[pos01][pos03];
				var price_envelop = num_envelop*data_arr[pos01][15];
				var ship = 1*data_arr[pos01][pos04];
				
				
				
				var price = amount_card + price_papertype + price_envelop + ship;
				
				jQuery('#result input').val(price.toFixed(2));
			});
			
		} else {
			//jQuery('#message').css('display','block');
			//jQuery('#message').append(message);
		}	
	}

jQuery(function(){	
    //jQuery('.button').click(function(){	
		//alert("fdfdf");
		//calprice();
	//});
	calprice('first');
	jQuery('#type').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice('last');   
		}
    });
	jQuery('#orien').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice('last');   
		}
    });
	jQuery('#size').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice('last');   
		}
    });
	jQuery('#papertypes').change(function(e){
       if($("#amount-envelop").val() >= 0 || $("#amount-card").val() >= 0){
			calprice('last');   
		}
    });
	
	$('#amount-card').keyup(function () {
		calprice('last');   
	});
	$('#amount-envelop').keyup(function () {
		calprice('last');   
	});
	
	$(".amount input[type='text']").each(function(index, element) {
		  placeHolder($(this));     
	});
	
	
	$('#amount-card, #amount-envelop').keydown(function(event) {
		// Allow special chars + arrows 
		if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
			|| event.keyCode == 27 || event.keyCode == 13 
			|| (event.keyCode == 65 && event.ctrlKey === true) 
			|| (event.keyCode >= 35 && event.keyCode <= 39)){
				return;
		}else {
			// If it's not a number stop the keypress
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	
});



$(document).ready( function() {
	 
	
	$("#type").change(function() {
		//hide social worker and sponsor stuff
		//$('#orien').parent().find('.select').html(''); 
		//$('#size').parent().find('.select').html(''); 
		val = $(this).val();
		//alert(val);
		if (val == 'enkel') {
			//$("#orien option[value='square']").attr("selected", "selected");
			//$("#orien > [value='square']").attr("selected", "true");
			//$('#orien option').hide(); 
			//$('#orien option').eq(1).show(); 
			
		} else {
			
		 	//$('#orien option').show();
			//$('#orien option').eq(0).attr("selected", "true");
			//$('#orien option').eq(2).hide(); 
		}
	});
	
	
	$("#orien").change(function() {
		$('#size').parent().find('.select').html(''); 
	});
	
	
    //$("#total").val("0");
    $("#amount").val("50");
    $("#amountb").val("50");
    $( function() {
		
		
        

        $("#slider").slider({
            value: "50",
           	min: 0,
            max: 200,
            step: 1,
            slide: function(event, ui) {
				var label = '#amount-card';
				$(label).val(ui.value).position({
					my: 'center top',
					at: 'center bottom',
					of: ui.handle,
					offset: "75, -34"
				});
				calprice('last');
				
                //$("#price").val(t[ui.value]);
                
                
                //$("#total").val(+aaa + +bbb);
            }
        });

        $("#sliderb").slider({
            value: "50",
            min: 0,
            max: 200,
            step: 1,
            slide: function(event, ui) {
				var label = '#amount-envelop';
				$(label).val(ui.value).position({
					my: 'center top',
					at: 'center bottom',
					of: ui.handle,
					offset: "75, -34"
				});
                calprice('last');
			}
        });
		
		
		$('#amount-card').val($('#slider').slider('value', 50)).position({
			my: 'center top',
			at: 'center bottom',
			of: $('#slider a:eq(0)'),
			offset: "75, -34"
		});
		
		$('#amount-envelop').val($('#sliderb').slider('value', 50)).position({
			my: 'center top',
			at: 'center bottom',
			of: $('#sliderb a:eq(0)'),
			offset: "75, -34"
		});

    });

});