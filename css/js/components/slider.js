$(function () {
	var $slider = $('.slider');

	if ($slider.length){
		$slider.cycle({
			fx: 'fadeout',
			slides: '> a'
		});
	}
});