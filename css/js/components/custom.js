$(function () {

    var $header = $('#header');

    var $headerMenu = $header.find('.topbar-menu-right');
    $headerMenu.find('#navbar-basket').html($("#headerbasket #basket_count").html());
    if (jQuery("#headerlogout").length > 0) {
        logout = "<a href='/logout' title='Uitloggen' class='logout'><span>" + jQuery("#headerlogout span").html() + "</span></a>";
    }
    if (!$("#headerlogin").length) {
        $headerMenu.find('#navbar-login').remove();
    }

    if (!$("#headerlogout").length) {
        $headerMenu.find('#navbar-logout').remove();
    }

    // append visual content
    if ($header.find('#visual-content').length === 0) {
        $header.append('<div id="visual-content"></div>');
    }

});